const PREFIX = 'c!';

const CMD_NAZE = 'naze';
const CMD_RETARD = 'retard';
const CMD_PREFIX = 'prefixe';
const CMD_OSS = 'oss';
const CMD_BALAVOINE = 'balavoine';
const CMD_SHIFUMI = 'shifumi';
const CMD_CHIANT = 'chiant';
// id user Alex
const POLARIUS = '140898421547466752';
// is user Chris
const MAGIKARP = '175640252516532224';
// id user Birdy
const BIRDY = '341677565208952834';
// id user Corentin
const COCO = '246702241262600203';
var modechiant = false;

class MessageParse {

    constructor(messageItem) {

    }

    /**
     * Process de traitement des messages
     * @param messageItem
     */
    static process(messageItem) {
        //Pour voir si Birdy ouvre sa gueule
        if(messageItem.member.user.id === BIRDY && modechiant){
            let tagUser = messageItem.member.user.tag;
            MessageParse.sendMessage(messageItem, 'Tg stp <@'+BIRDY+'>');
        }

        if (MessageParse._shouldTreat(messageItem)) {
            MessageParse.treatCommand(messageItem);
        }
    }

    /**
     * Process de traitement des paramètres
     * @param messageItem
     * @param paramNumber
     * @returns {boolean|string}
     */
    static getParameter(messageItem, paramNumber = 0) {
        let command = messageItem.content.substr(2, messageItem.content.length);
        let values = command.split(" ");
        if (values.length > 0) {
            if (values[paramNumber]) {
                return values[paramNumber];
            }
        }
        return false;
    }

    /**
     * Détermine si le message doit être process par le bot ou non
     * @param messageItem
     * @returns {boolean}
     * @private
     */
    static _shouldTreat(messageItem) {
        // console info de l'id user
        console.info(messageItem.member.user.id);
        return messageItem.content.substr(0, 2) === PREFIX;
    }

    /**
     *
     * @param messageItem
     * @returns {boolean|*}
     */
    static treatCommand(messageItem) {
        messageItem.content = messageItem.content.toLowerCase();
        let command = MessageParse.getParameter(messageItem);
        let firstParameter = MessageParse.getParameter(messageItem, 1);
        switch (command) {
            case CMD_CHIANT:
                return MessageParse.handleChiantCMD(messageItem, firstParameter);
            case CMD_NAZE:
                return MessageParse.handleNazeCMD(messageItem);
            case CMD_SHIFUMI:
                return MessageParse.handleShifumiCMD(messageItem, firstParameter);
            case CMD_RETARD:
                return MessageParse.handleRetardCMD(messageItem);
            case CMD_PREFIX:
                return MessageParse.handlePrefixCMD(messageItem);
            case CMD_OSS:
                return MessageParse.handleOSSCMD(messageItem);
            case CMD_BALAVOINE:
                return MessageParse.handleBalavoineCMD(messageItem);
            default:
                MessageParse.sendMessage(messageItem, 'J\'ai pas compris (je suis un peu con désolé)');
                return true;
        }
    }

    /**
     *
     * @param messageItem
     * @param msgToSend
     */
    static sendMessage(messageItem, msgToSend) {
        if (msgToSend !== '') {
            messageItem.channel.send(msgToSend);
            return true;
        }
        return false;
    }

    /**
     *
     * @param messageItem
     * @param playerValue
     * @returns {boolean}
     */
    static handleShifumiCMD(messageItem, playerValue) {
        let possibleValues = [
            "papier",
            "pierre",
            "ciseaux"
        ];

        let userId = messageItem.member.user.id;

        let botWinStr = 'J\'ai gagné, cheh gros nul';
        let playerWinStr = 'Ton point faible ? Trop fort.';
        let drawStr = 'Egalité, on recommence ?';

        let botValue = possibleValues[Utils.getRandomInt(possibleValues.length)];
        MessageParse.sendMessage(messageItem, 'Je choisi '+ botValue);


        switch (userId) {
            case MAGIKARP:
                break;
            default:
                switch (playerValue) {
                    case 'papier':
                        if (botValue === 'papier') {
                            MessageParse.sendMessage(messageItem, drawStr);
                        } else if (botValue === 'pierre') {
                            MessageParse.sendMessage(messageItem, playerWinStr);
                        } else if (botValue === 'ciseaux') {
                            MessageParse.sendMessage(messageItem, botWinStr);
                        }
                        break;

                    case 'pierre':
                        if (botValue === 'papier') {
                            MessageParse.sendMessage(messageItem, botWinStr);
                        } else if (botValue === 'pierre') {
                            MessageParse.sendMessage(messageItem, drawStr);
                        } else if (botValue === 'ciseaux') {
                            MessageParse.sendMessage(messageItem, playerWinStr);
                        }
                        break;

                    case "ciseaux":
                        if (botValue === 'papier') {
                            MessageParse.sendMessage(messageItem, playerWinStr);
                        } else if (botValue === 'pierre') {
                            MessageParse.sendMessage(messageItem, botWinStr);
                        } else if (botValue === 'ciseaux') {
                            MessageParse.sendMessage(messageItem, drawStr);
                        }
                        break;

                    case "bite":
                        if (botValue === 'papier') {
                            MessageParse.sendMessage(messageItem, "Et je t'enrobe le péni avec ce papier de soie");
                        } else if (botValue === 'pierre') {
                            MessageParse.sendMessage(messageItem, "Et je t'écrase le péni");
                        } else if (botValue === 'ciseaux') {
                            MessageParse.sendMessage(messageItem, "Et je te découpe le péni");
                        }
                        break;

                    default:
                        MessageParse.sendMessage(messageItem, "Le principe c'est de mettre pierre, papier ou ciseaux, pas autre chose ... Débile.");

                }
        }

    }

    // Handle le c!chiant birdy
    static handleChiantCMD(messageItem, nameValue){

        if(messageItem.member.user.id === MAGIKARP || messageItem.member.user.id === POLARIUS){
        
        if(nameValue === 'birdy'){
            if(modechiant === false){
                modechiant = true;
                MessageParse.sendMessage(messageItem, 'Mode chiant pour Birdy activé');
                modechiant = true;
                return true;
            }
            else if(modechiant === true){
                modechiant = false;
                MessageParse.sendMessage(messageItem, 'Mode chiant pour Birdy désactivé');
                modechiant = false;
                return false;
            }
        }
    }
    else{MessageParse.sendMessage(messageItem, 'T\'as pas le droit de faire ça, t\'es pas dictateur ou t\'es pas Polarius');}

    }

    static handleOSSCMD(messageItem) {
        let possibleValues = [
            "J'aime me battre",
            "J'aime me beurrer la biscotte",
            "C'est toi arrêtez !",
            "A l'occasion, j'vous mettrai un petit coup de polish",
            "Non mais oh ! Comment tu parles de ton père ?",
            "23 à 0 ! C'est la piquette Jack !",
            "Tu sais pas jouer, T'ES MAUVAIS",
            "J'aime quand une jolie brune aux yeux marrons m'apporte mon petit déjeuner au lit",
            "FONCE SLIMANE",
            "C'est René Coti, et c'est ton ami",
            "Serpent ! Je ne mange pas de ce pain là"
        ];

        let chosenValue = possibleValues[Utils.getRandomInt(possibleValues.length)];
        if (chosenValue) {
            MessageParse.sendMessage(messageItem, chosenValue);
            return true;
        } else {
            MessageParse.handleOSSCMD(messageItem);
        }
    }

    /**
     * Renvoie une phrase aléatoire de Jérôme
     * @param messageItem
     * @returns {boolean}
     */
    static handleNazeCMD(messageItem) {
        let possibleValues = [
            'On va attendre 4 jours d\'avoir les droits admin sur les postes',
            'Ma voiture est en panne',
            'Alors on va faire une table "Personne" ...',
            'Je suis malade',
            'Ma fille est malade',
            'Vous vous en sortez ?',
            'On prends une petite pause ?',
            'Je vais me chercher un café',
            'Vous allez lancer Visual Studio, je reviens vers vous dans 1 heure',
            "Vous aimez bien les fourmis ?"
        ];

        let chosenValue = possibleValues[Utils.getRandomInt(possibleValues.length)];

        if (chosenValue) {
            MessageParse.sendMessage(messageItem, chosenValue);
            return true;
        } else {
            MessageParse.handleNazeCMD(messageItem);
        }
    }

    static handleBalavoineCMD(messageItem) {
        let possibleValues = [
            "C'EST MON FILS MA BATAILLE",
            "FALLAIT PAS QU'ELLE S'EN AILLE",
            "OH JE VAIS TOUT CASSEEEEEEEEEEER",
            "SI VOUS TOUCHEEEEEEZ AU FRUIT DE MES ENTRAILLES",
            "LES JUGES ET LES LOOOIIIIIS, CA M'FAIT PAS PEUUUUUR",
            "WOOOOHOUHOOOOOO"
        ];

        let chosenValue = possibleValues[Utils.getRandomInt(possibleValues.length)];

        if (chosenValue) {
            MessageParse.sendMessage(messageItem, chosenValue);
            return true;
        } else {
            MessageParse.handleBalavoineCMD(messageItem);
        }
    }

    static handleRetardCMD() {



    }

    /**
     *
     * @returns {boolean}
     */
    static handlePrefixCMD(messageItem) {
        MessageParse.sendMessage(messageItem, "Le prefixe du bot est " + PREFIX + ', pas touche.');
        return true;
    }

}

module.exports = MessageParse;
global.MessageParse = MessageParse;