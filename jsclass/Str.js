/**
 *
 */
class Str {

    /**
     * Retourne un texte gras (markdown)
     * @param string
     * @returns {string}
     */
    static bold(string) {
        return "**" + string + "**";
    }

    /**
     * Retourne un texte italique (markdown)
     * @param string
     * @returns {string}
     */
    static italic(string) {
        return "*" + string + "*";
    }

    /**
     * Retourne une texte barré (markdown)
     * @param string
     * @returns {string}
     */
    static linethrough(string) {
        return "~~" + string + "~~";
    }

    /**
     * Retourne un texte souligné (markdown)
     * @param string
     * @returns {string}
     */
    static underline(string) {
        return "__" + string + "__";
    }
}

module.exports      = Str;
global.Str          = Str;
